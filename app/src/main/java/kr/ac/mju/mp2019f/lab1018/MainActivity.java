package kr.ac.mju.mp2019f.lab1018;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private MediaPlayer mp;
    private Button btPlay;
    private SeekBar sbPlay;
    private TextView tvCurrent;
    private SimpleDateFormat df;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mp = MediaPlayer.create(getApplicationContext(), R.raw.napalbaji);


        btPlay = (Button) findViewById(R.id.btPlay);
        sbPlay = (SeekBar) findViewById(R.id.seekBar);
        tvCurrent = (TextView) findViewById(R.id.tvCurrent);

        sbPlay.setMax(mp.getDuration());
        df = new SimpleDateFormat("mm:ss");

        sbPlay.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(b && mp != null) {
                    mp.seekTo(i);
                    tvCurrent.setText(df.format(new Date(i)));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mp==null) return;

                if(mp.isPlaying()) {
                    // pause
                    mp.pause();
                    btPlay.setText("Play");
                } else {
                    // play
                    mp.start();
                    btPlay.setText("Pause");

                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if( mp != null ) {
            mp.stop();
            mp.release();
            mp = null;
        }
    }
}
